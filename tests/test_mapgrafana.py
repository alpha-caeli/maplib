import maplib
from datetime import datetime, timedelta
import json


def test_mapgrafana():

    g = maplib.Grafana()
    # cleanup
    try:
        g.delete_dashboard("unittest_dash")
    except:
        pass

    # list the dash
    assert not g.dashboard_exists("unittest_dash")

    # create a tmp one
    dash = {
        "id": None,
        "uid": None,
        "title": "unittest_dash",
        "tags": ["templated"],
        "timezone": "browser",
        "schemaVersion": 16,
        "version": 0,
        "refresh": "25s"
    }
    assert g.push_dashboard(dash, True)

    # check for it
    assert g.dashboard_exists("unittest_dash")

    # delete it
    g.delete_dashboard("unittest_dash")

    # check for it
    assert not g.dashboard_exists("unittest_dash")


def test_dashboard_push():
    g = maplib.Grafana()
    dash = {
        "id": None,
        "uid": None,
        "title": "unittest_dash",
        "tags": ["templated"],
        "timezone": "browser",
        "schemaVersion": 16,
        "version": 0,
        "refresh": "25s"
    }
    try:
        g.delete_dashboard("unittest_dash")
    except:
        pass
    with open("dash.json", "w+") as fd:
        json.dump(dash, fd)
    g.update_dashboard("dash.json", "testing")
    assert "testing" in g.list_folders()


def test_annotations():
    g = maplib.Grafana()
    g.create_annotation(text="broken now", tags=["ping"])
    yesterday = datetime.now() - timedelta(days=1)
    g.create_annotation(
        time=yesterday, text="was broken yesterday", tags=["ping"])
