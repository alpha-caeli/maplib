from maplib.mapkapacitor import _get_tasks, upsert_task
import maplib
import os
from pprint import pprint

os.environ["MAPLIB_LOG_LEVEL"] = "DEBUG"


def test_get_tasks():

    t = _get_tasks()
    assert isinstance(t, list)


def test_upsert_task():
    tick_script = """
    dbrp "map"."autogen"
    var rtt_min = 3
    stream
        |from()
            .measurement('ping')
        |alert()
            .crit(lambda: int("rtt_min") >=  rtt_min)
            .stateChangesOnly()
            .post('http://hubble:5000/hubble/events/kapa')
    """
    upsert_task("ping-casse", tick_script,
                {"rtt_min": {"type": "int", "value": 2}})
