.. maplib documentation master file, created by
   sphinx-quickstart on Thu Jan 28 11:49:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to maplib's documentation!
==================================

:Release: |version|

.. toctree::
   :maxdepth: 4

   readme
   maplib

Unittests & codecov
===================
.. toctree::
  
  pytest_report

Development
===========
.. toctree::
   
   pylint_report

* :ref:`genindex`
