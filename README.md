## maplib

__maplib__ is a wrapper for services in MAP


![easter1](https://img.shields.io/badge/not_a_bug-a_feature-purple)
![easter2](https://img.shields.io/badge/compatibility-windows98-orange)
![pythonshield](https://img.shields.io/badge/requires-python>=3.8-blue)


![easter3](https://img.shields.io/badge/current_version-1.7.1-green)
![easter4](https://img.shields.io/badge/deprecated-<1.5-red)


Documentation available here : https://alpha-caeli.gitlab.io/maplib/index.html


It provides autoconfigured modules for the following services available in MAP: 

- influx
- vault
- netbox
- loki 
- hasura
- grafana
- hubble
- minio

And a logging module for standardized logging which can upload errors to sentry.


## Installation

```sh
# using versionning
pip3 install maplib==1.7.1 --extra-index-url https://gitlab.com/api/v4/projects/27272438/packages/pypi/simple

#rolling release
pip3 install git+https://gitlab.com/alpha-caeli/maplib.git
```

## Configuration

There are various methods for configuring __maplib__.

Either through code when things are not expected to change, or through environment variables. You'll find relevant information about the arguments you can pass to each module, or the environment variable it accepts in the documentation of that particular module.

## Usage

This is a very basic example of how a microservice _should_ use maplib. A better way to learn is to look at the ping microservice [here](https://gitlab.com/alpha-caeli/microservices/ping). It's basic but sets a good example.

Starting with maplib 1.6.0, it's best practice to use the `.init()` function in your entry point. This will preload some stuff & configure the loggers properly.

The first argument is the slug of your microservice, e.g. _snmp-interfaces_. This will appear in Netbox as a tag.
The optionnal second argument is the privileged mode of your microservice.

```python
# global import, or use from maplib import <submodule> 
import maplib

# configures a microservice, set to True if you need privileged access 
maplib.init("my-microservice",set_privileged=False)

# you can create loggers for everything
main_logger = maplib.Logger("main")
main_logger.info("lets go!")

# then you'll most certainly want to get a list of targets that have your tag:
hasura = maplib.Hasura()
devices = hasura.retrieve_hosts_tagged_with("my-microservice")

for d in devices:
    print(d)

    # if you need creds, use Vault
    vault = maplib.Vault()
    api_keys = vault.read_secret("api/{}".format(d["ip"]))

    # you might want to signal hubble about something that went right (or wrong)
    maplib.hubble.success("group-name","message","hostname") # or .error(), .warning()

    # you might want to push metrics to influx
    influx = maplib.Influx()
    influx.push(stats) # stats is a point or list of points. See influx section for more details
```

## Privileged mode

Vault works with roles, and policies attached to these roles.
Currently 2 roles are defined in maplib, `microservice` and `privileged`.

They are tied to specific serviceAccount in Kubernetes. By default pods are bound to the default serviceAccount, which is tied to the `microservice` role.

It means that to be able to have the `privileged` role in vault, your pod will need the correct serviceAccount token.

If you are indeed a privileged pod, with the correct serviceAccount, 
you'll need to tell maplib about it by enabling the privileged arg in the `.init()` method. 


## Code examples

Here are a few snippets to get you started with each module. For a more in-depth usage, refer yourself to the code documentation or look through the existing microservices !

### maplog

This module standardize logging. You can declare multiple loggers in your app (eg for each device or each thread)

```python
import maplib

logger = maplib.Logger("myapp")
logger.info("hello")
```

### mapinflux

Wrapper around influxdb to push points.

```python
import maplib

i = maplib.Influx()
p = {"measurement":"ping",
     "tags":{"ip":"10.0.1.2"},
     "fields":{"avg":0.027639,
               "min":0.01373,
               "max":0.28983}}
i.push(p)
```

### maploki (deprecated)

```python
import maplib

l = maplib.Loki("myapp")
l.info("a line pushed to loki")
l.error("an error line pushed to loki")
```

### mapminio

```python
import maplib

m = maplib.Minio()
m.create_bucket("files")
m.upload_file("./file.txt","files","file.txt")
```

### mapnetbox

Interact with MAP's source of truth

```python
import maplib

n = maplib.Netbox()
t = n.retrieve_hosts_with_tag("ping") # use maplub.Hasura().retrieve_hosts_tagged_with() since 1.6.0
```

### maphasura

Make queries to hasura graphQL engine.

```python
import maplib

h = maplib.Hasura()
rep = h.query("query q {dcim_device{name}}")
devices = h.retrieve_hosts_tagged_with("ms")
```

### mapvault

retrieve secrets from the vault.

```python
import maplib

v = maplib.Vault()
s = v.read_secret("ssh/generic")
```

### mapgrafana

retrieve & create grafana dashboards.

```python
import maplib

dash = { 
    "id": None,
    "uid": None,
    "title": "my dash",
    "timezone": "browser",
    "schemaVersion": 16,
    "version": 0,
    "refresh": "25s"
}

g = maplib.Grafana()
if not g.cached_dashboard_exists("my-ms"):
    g.push_dashboard(dash)
```

### maphubble

Send events to Hubble.

```python
from maplib import hubble

hubble.success("ping","device is online","switch1")
hubble.error("ping","device is offline","switch2")
```
