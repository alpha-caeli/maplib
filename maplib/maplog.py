"""Module for standardized logging

Create a logger with
```
my_log = maplib.Logger("my_app")
```

It supports methods like a regular Logging instance :

- .debug()
- .info()
- .warning()
- .error()
- .critical()
- .exception()

Environment Variables:

- `MAPLIB_LOG_LEVEL`: This will set the level to the given level for every module.
sentry error reporting will be automatically configured if the DNS key is present in vault (/services/sentry/dsn) when the first logger is initialized.
"""
import logging
import os

import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration

_strLevelToLogLevel = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL
}


def Logger(name, level='INFO'):
    """Creates a logger object that can be used to format log output

    If the `MAPLIB_LOG_LEVEL` env var is a valid log level, the level is overriden to its value.

    Args:
        name (str, optional): the name of the app using the module. Defaults to 'app'.
        level (str, optional): Level of the messages to log.
        Can be DEBUG, INFO, WARNING, ERROR, CRITICAL. Defaults to 'INFO'.

    Returns:
        logging.Logger: A logger object
    """

    # Create a custom logger
    log = logging.getLogger(name)

    # as we might be in a threaded environment, we need to make sure that we do not add a handler
    # every time we spawn a new thread with a logger object.
    log.propagate = False
    if log.hasHandlers():
        return log

    # add a streamhandler for stdout
    stdout_handler = logging.StreamHandler()

    # Format it
    stdout_format = logging.Formatter(
        fmt='%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S")
    stdout_handler.setFormatter(stdout_format)
    log.addHandler(stdout_handler)

    # set default log level, or from the MAPLIB_LOG_LEVEL if it's a valid level (see _strLevelToLogLevel dict)
    maplib_env_level = os.getenv("MAPLIB_LOG_LEVEL", "")
    if maplib_env_level.upper() in _strLevelToLogLevel:
        log.setLevel(maplib_env_level)
    else:
        log.setLevel(level)

    # return the logger object
    return log


def sentry_setup(microservice_name: str):
    """configures sentry error reporting if the dsn secret is present in vault

    the secret is directly mapped to arguments of the sentry constructor. 
    That leave more configuration available by just adding keys to the secret, like `environnement`.. 

    note that we import vault here to avoid an infinite import loop.

    Args:
        microservice_name (str): [description]
    """
    # whatever happens here, we don't want to break upper level code
    try:
        from .mapvault import Vault
        logger = Logger("sentry")
        # we don't care for vault's status at this point
        vault = Vault(quiet=True)
        dsn = vault.read_secret("services/sentry/dsn")
        if dsn:
            # add the commit sha release from the env if present
            commit_sha = os.getenv("COMMIT_SHA")
            if commit_sha:
                dsn["release"] = commit_sha
            sentry_sdk.set_tag("microservice", microservice_name)
            sentry_logging = LoggingIntegration(
                level=logging.WARNING,
                event_level=_strLevelToLogLevel.get(
                    os.getenv("MAPLIB_SENTRY_LEVEL", "ERROR").upper()))
            sentry_sdk.init(**dsn,
                            integrations=[sentry_logging])
            logger.debug("sentry configured !")
    except:
        pass
