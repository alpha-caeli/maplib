echo "cleaning up"
make clean

echo "converting markdown to rst"
pandoc -f markdown -t rst -o readme.rst ../README.md

echo "generating code report"
pylint -d C0301,C0303 -f text -r y ../maplib/*.py | awk  '/Report/,/EOF/' > pylint_report.rst

echo "generating pytest & codecov report"
pytest --cov=maplib ../tests/ > pytest_report.rst
sed -i 's/^/  /' pytest_report.rst
sed -i '1s/^/pytest\n------\n::\n/' pytest_report.rst

echo "building html"
make html

# if this is a ci job
[[ $1 = "ci" ]] && echo "moving build folder to /public" && mv _build/html ../public

