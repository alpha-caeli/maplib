FROM python:3.10-alpine
WORKDIR /build
COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .
RUN pip3 install .
RUN rm -rf /build
WORKDIR /app