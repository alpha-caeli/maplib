"""testing module

> pytest -s  -k <submodule>
"""
from datetime import datetime, timedelta
from random import choices, randint
from string import ascii_lowercase

import maplib


def test_minio():
    m = maplib.Minio()
    bucket_name = "unittest-"+"".join(choices(ascii_lowercase, k=10))
    l = m.list_buckets()
    assert isinstance(l, list)

    m.create_bucket(bucket_name)
    l2 = m.list_buckets()
    assert len(l2) == len(l)+1

    with open("file.txt", "w+") as fd:
        fd.write("hello")
    m.upload_file("file.txt", bucket_name, "yolo")

    f = m.list_files(bucket_name)
    assert len(list(f)) == 1

    file = m.retrieve_file(bucket_name, "yolo")
    assert file.decode() == "hello"

    m.client.remove_object(bucket_name, "yolo")
    m.client.remove_bucket(bucket_name)
    assert len(m.list_buckets()) == len(l)
