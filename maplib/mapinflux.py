"""Module around InfluxDBClient.

You can invoke a client using maplib.Influx().
By default it will be autoconfigured through Vault, but you can specify `autoconfig=False` and pass arguments as you would with a normal InfluxDBClient instance.

Also a `retention_policy` argument ca be passed to specifiy the retention policy to use.

Environment Variables :

- `MAPLIB_INFLUX_RETENTION_POLICY` : overwrites the retention policy.
"""
import os

from influxdb import InfluxDBClient

from .maplog import Logger
from .mapvault import Vault


class Influx(InfluxDBClient):
    """A overwrite of the influx client that supports autoconfiguration using vault
    """

    def __init__(self, autoconfig=True, retention_policy=None, **kwargs):
        """init with either autoconfig to true (default), or just pass the arguments as you would on a regular InfluxDBClient instance

        Args:
            autoconfig (bool, optional): triggers autoconfig in a map env. Defaults to True.
        """

        self.logger = Logger("influx")

        # config
        if autoconfig:
            self.logger.debug("configuring automagically !")
            vault = Vault()
            if vault.authenticate():
                secret = vault.get_influx_secrets()
                super().__init__("influx", 8086,
                                 secret["user"], secret["password"], secret["db"])
            else:
                self.logger.critical(
                    "Autoconfig is on but vault is not accessible !")
        else:
            self.logger.debug("configuring through args..")
            super().__init__("influx", 8086, **kwargs)

        # check first for the env var
        policies = [x["name"] for x in self.get_list_retention_policies()]
        if "MAPLIB_INFLUX_RETENTION_POLICY" in os.environ:
            self.logger.debug("Env var for RP is set ! ")
            if os.environ["MAPLIB_INFLUX_RETENTION_POLICY"] in policies:
                self.retention_policy = os.environ["MAPLIB_INFLUX_RETENTION_POLICY"]
            else:
                self.logger.warning(
                    "Retention Policy '%s' set by env var is not correctly spelled !",
                    os.environ["MAPLIB_INFLUX_RETENTION_POLICY"])
                self.logger.warning("Possible RP are %s", policies)
                self.logger.warning("Falling back to the default RP")
                self.retention_policy = None

        # otherwise arg
        elif retention_policy and retention_policy in policies:
            self.retention_policy = retention_policy

        # otherwise None
        else:
            self.retention_policy = None

    def push(self, point_or_points):
        """push a point or list of points to influx

        Args:
            point_or_points (dict or list of dicts): the points to push
        """
        if isinstance(point_or_points, dict):
            self.write_points(
                [point_or_points],
                retention_policy=self.retention_policy)
        elif isinstance(point_or_points, list):
            self.write_points(
                point_or_points,
                retention_policy=self.retention_policy)
        else:
            self.logger.error(
                "argument should be a point (dict) or list of points (list), was %s", type(point_or_points))
