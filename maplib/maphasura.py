"""Module that makes graphQL queries to hasura

Create an object with  `malib.Hasura()`.

By default it will be configured with a token provided by vault
and the default hasura url. You can change that by setting autoconfig=False and setting your arguments.

Environment variables :

- `MAPLIB_HASURA_URL`: the full url for hasura (including proto & path)
- `MAPLIB_HASURA_TOKEN`: the token for hasura auth

"""
import os
from typing import Any

import requests
from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport
from graphql import error as gqlError

from .maplog import Logger
from .mapvault import Vault

requests.urllib3.disable_warnings()


class Hasura():
    """A hasura client for making graphQL queries
    """

    def __init__(self, autoconfig=True, url="", token="",
                 secret_path="services/hasura/token"):
        """Creates a Hasura instance

        Args:
            autoconfig (bool, optional): Controls whether configuration is done through vault. Set to False if you change the next args Defaults to True.
            url (str, optional): the full url for hasura (ex http://hasura/v1/graphql). Defaults to "".
            token (str, optional): The auth token for hasura. Defaults to "".
            secret_path (str,optionnal): the vault path where the hasura token is stored, in a {"token":"xx"} format. Defaults to "services/hasura/token".
        """
        self.logger = Logger("hasura")

        # env var
        if all([os.getenv("MAPLIB_HASURA_URL"),
               os.getenv("MAPLIB_HASURA_TOKEN")]):
            self.logger.debug("env var detected, configuring through env vars")
            self.url = os.getenv("MAPLIB_HASURA_URL")
            self.token = os.getenv("MAPLIB_HASURA_TOKEN")

        # auto
        elif autoconfig:
            self.logger.debug("configuring automagically !")
            vault = Vault()
            if vault.authenticate():
                token = vault.read_secret(secret_path)
                if token:
                    try:
                        self.token = token["token"]
                    except KeyError:
                        self.logger.error(
                            "The key 'token' is missing from the secret !")
                        self.token = None  # deliberalty set to None so subsequent methods will fail
                        return
                else:
                    self.token = None  # same here
                    return
                self.url = "http://hasura/v1/graphql"
            else:
                self.logger.critical("vault is not accessible !")
        # args
        else:
            self.logger.debug("configuring through args..")
            self.url = url
            self.token = token

        self.headers = {"x-hasura-admin-secret": self.token}
        self.transport = RequestsHTTPTransport(
            url=self.url, headers=self.headers, verify=False)
        self.client = Client(transport=self.transport)

    def query(self, query: str, variables_values=None) -> Any:
        """sends a query to hasura. Supports query params like th og client (see variables_values)

        Args:
            query (str): raw query to send to hasura
            variables_values (dict, optional): Optionnal parameters for the query in dict format. Defaults to None.

        Returns:
            Any: dict returned by hasura if the query was successful, false otherwise
        """
        # parse
        try:
            gql_query = gql(query)
            self.logger.debug("Query parsed without issues")
        except gqlError.syntax_error.GraphQLSyntaxError as syntax_error:
            self.logger.error("The query was malformed : %s", syntax_error)
            return None

        # exec
        try:
            response = self.client.execute(gql_query, variables_values)
        except requests.exceptions.RequestException as http_error:
            self.logger.error(
                "Something failed when making the request : %s", http_error)
            return None
        except Exception as unknown_exception:
            self.logger.error(
                "An unknown error appeared when making the request: %s", unknown_exception)
            return None
        return response

    def retrieve_hosts_tagged_with(self, tag: str) -> list:
        """retrieve devices from netbox which have the given tag

        Args:
            tag (str): name of the tag to search device for

        Returns:
            list: list of {} for each device
                  as [
                      {
                          "id":int,
                          "name":str,
                          "status":str,
                          "site":str,
                          "manufacturer":str,
                          "ip":str or None,
                          "role":str,
                          "device_type":str,
                          "serial":str,
                      },
                  ]
        """

        devices_query = """query MyQuery($tag: String) {
                            dcim_device(where: {device_tags: {extras_tag: {slug: {_eq: $tag}}}}) {
                                id
                                name
                                serial
                                status
                                dcim_site { name }
                                dcim_devicetype {
                                  slug
                                  dcim_manufacturer { slug } 
                                  }
                                dcim_devicerole { slug }
                                ip_address { address }
                                }
                            }
                            """
        r = self.query(devices_query, {"tag": tag})
        if r:
            formatted_devices = []
            devices = r["dcim_device"]
            for d in devices:
                formatted_devices.append({"id": d["id"],
                                          "name": d["name"],
                                          "status": d["status"],
                                          "site": d["dcim_site"]["name"],
                                          "ip": (d["ip_address"] or {}).get("address", "").split("/")[0] or None,
                                          "manufacturer": d["dcim_devicetype"]["dcim_manufacturer"]["slug"],
                                          "role": d["dcim_devicerole"]["slug"],
                                          "device_type": d["dcim_devicetype"]["slug"],
                                          "serial": d["serial"]})
            return formatted_devices
        return None
