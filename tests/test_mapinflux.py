import maplib
from random import randint


def test_mapinflux():

    i = maplib.Influx()
    assert isinstance(i.ping(), str)
    assert isinstance(i.get_list_database(), list)

    # push a single point
    rng = randint(0, 1000)
    i.push({"measurement": "unittest", "tags": {
           "rng": rng}, "fields": {"success": 1}})
    r = i.query(
        'SELECT mean("success") FROM "unittest" WHERE ("rng" = \''+str(rng)+'\')')
    assert len(list(r.get_points())) >= 1

    # push multiple points
    rng = randint(0, 1000)
    i.push([{"measurement": "unittest", "tags": {
           "rng": rng}, "fields": {"success": 1}}])
    r = i.query(
        'SELECT mean("success") FROM "unittest" WHERE ("rng" = \''+str(rng)+'\')')
    assert len(list(r.get_points())) >= 1

    i.drop_measurement("unittest")
