"""This module provides wrapper to send events to hubble.
Events are messages regarding a device. They can be errors, warnings or even successes.

By default, the hubble endpoint is `http://hubble:5000/hubble`, but can be changed by setting `maplib.hubble.hubble_endpoint` or the `MAPLIB_HUBBLE_ENDPOINT` env var.
"""
import os

import requests

from .maplog import Logger


log = Logger("hubble")
hubble_session = requests.Session()

hubble_endpoint = os.getenv(
    "MAPLIB_HUBBLE_ENDPOINT",
    "http://hubble:5000/hubble")


def get_thresholds(ms_name: str) -> dict:
    """get the thresholds from hubble

    Args:
        ms_name (str): tag of the microservice

    Returns:
        dict: dict with values for the thresholds if set, None otherwise
    """
    try:
        thresholds_requests = requests.get(
            f"{hubble_endpoint}/thresholds/tag/{ms_name}")
    except Exception as req_err:
        log.error(req_err)
    if thresholds_requests.status_code == 200:
        return thresholds_requests.json()
    # if it's not 200 it would be 404 meaning we don't have a threshold set
    return None


def device_event(group: str, message: str, status: str, hostname: str):
    """send a new device event to hubble

    Args:
        group (str): the group of the alert (usually the microservice name)
        message (str): arbitrary message related to the event
        status (str): new device status, can be (active, failed)
        hostname (str): hostname of the device
    """
    log.debug("Sending event to hubble %s,%s,%s", message, status, hostname)
    try:
        rep = hubble_session.post(
            f"{hubble_endpoint}/events/device",
            json={
                "group": group,
                "message": message,
                "status": status,
                "hostname": hostname})
        if not rep.ok:
            log.error(
                "Hubble didn't replied 200 : %s: %s",
                rep.status_code,
                rep.text)
    except requests.RequestException as http_err:
        log.error("Unable to send event to hubble : %s", http_err)


def send_message(group, message, level, hostname):
    """send a given event to hubble

    Args:
        group (str): tag to group events, should be the name of the microservice
        message (str): actual message
        level (str): `success`,`warning` or `error`
        hostname (str): device affected by the event
    """
    log.debug("Sending event to hubble %s,%s,%s", message, level, hostname)
    try:
        rep = hubble_session.post(
            f"{hubble_endpoint}/events",
            json={
                "group": group,
                "message": message,
                "level": level,
                "hostname": hostname})
        if not rep.ok:
            log.error(
                "Hubble didn't replied 200 : %s: %s",
                rep.status_code,
                rep.text)
    except requests.RequestException as http_err:
        log.error("Unable to send event to hubble : %s", http_err)


def success(group, message, hostname):
    """send a success event to hubble

    Args:
        group (str): tag to group events, should be the name of the microservice
        message (str): actual message
        hostname (str): device affected by the event
    """
    send_message(group, message, "success", hostname)


def warning(group, message, hostname):
    """send a warning event to hubble

    Args:
        group (str): tag to group events, should be the name of the microservice
        message (str): actual message
        hostname (str): device affected by the event
    """
    send_message(group, message, "warning", hostname)


def error(group, message, hostname):
    """send an error event to hubble

    Args:
        group (str): tag to group events, should be the name of the microservice
        message (str): actual message
        hostname (str): device affected by the event
    """
    send_message(group, message, "error", hostname)


def send_topic(topic, message):
    """sends a message to an arbitrary topic to hubble

    Args:
        topic (str): name of the topic
        message (str): message
    """
    log.debug("Sending new message to topic '%s' to hubble", topic)
    try:
        rep = hubble_session.post(
            f"{hubble_endpoint}/events/topic",
            json={
                "topic": topic,
                "message": message})
        if not rep.ok:
            log.error(
                "Hubble didn't replied 200 : %s: %s",
                rep.status_code,
                rep.text)
    except requests.RequestException as http_err:
        log.error("Unable to send event to hubble : %s", http_err)
