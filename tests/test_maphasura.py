import maplib


def test_maphasura():
    # test a simple query
    ha = maplib.Hasura()
    query = "query test {dcim_device {name ip_address{address} status}}"
    rep = ha.query(query)
    assert isinstance(rep, dict)

    # test a query with params
    param_query = "query MyQuery($site_id: bigint_comparison_exp = {}) { dcim_device(where: {site_id: $site_id}) {name}}"
    params = {"site_id": {"_eq": 1}}
    rep = ha.query(param_query, params)
    assert isinstance(rep, dict)

    # malformed queries
    # We should expect to fail during the parsing, catch it and return None
    malformed_query = "query test {dcim_device {name ip_address{addr"
    badrep = ha.query(malformed_query)
    assert badrep is None
    bad_query = "query test {dcim_device {name ip_address{address status}}"
    badrep = ha.query(bad_query)
    assert badrep is None
