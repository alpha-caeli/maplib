"""testing module

> pytest -s  -k <submodule>
"""
from datetime import datetime, timedelta
from random import choices, randint
from string import ascii_letters
from maplib import internals
import maplib


def test_maphubble():
    maplib.hubble.warning("ping", "ping avg is high", "test-device")
    maplib.hubble.success("backup", "backup was made", "test-device")
    maplib.hubble.error("ping", "device offline", "test-device")
    maplib.hubble.send_topic("coco-is-watchin", "hello")


def test_init():
    t = "unittest-"+"".join(choices(ascii_letters, k=5))
    maplib.init(t)
    logger = maplib.Logger(t)
    logger.info("hello, testing info")
    logger.warning("watch out, testing warning")
    logger.error("help me, testing error")


def test_privileged_init():
    maplib.init("unittest-privileged", True)
    assert maplib.mapvault.ROLE == "privileged"


def test_internals():
    @internals.deprecated("not used anymore")
    def not_used_anymore():
        pass

    @internals.work_in_progress("a wip")
    def wip():
        pass

    @internals.time_cache()
    def cached():
        pass

    not_used_anymore()
    wip()
    cached()



def test_loki(no_cover):
    l = maplib.Loki("yolo")
