"""functions related to security
"""
import os
from functools import wraps

import requests

from .maplog import Logger

# if maplib is imported in a microservice without flask, we won't be able to
# use the function so we don't even bother declaring the func
try:
    from flask import request
except ImportError:
    pass
else:
    AUTH_ENDPOINT = os.getenv(
        "AUTH_ENDPOINT", "http://api:5000/api/login/auth")
    logger = Logger("api-security")
    logger.debug("Auth endpoint is %s", AUTH_ENDPOINT)

    def portal_auth_required(route):
        """This decorator provides authentification for routes that needs to be authenticated against the main API portal.
        This will only affect requests coming from outside the cluster, which are selected by their `x-forwarded-for` header.
        It means that in-cluster microservices will not have to authenticate, but requests coming from the ingress will.
        """
        @wraps(route)
        def wrapper(*args, **kwargs):
            # if we are in-cluster, we do not need auth
            # we rely on the ingress to provide the x-forwarder-for header for external clients
            if "X-Forwarded-For" not in request.headers:
                return route(*args, **kwargs)

            # otherwise we must auth against the portal
            if "Authorization" not in request.headers:
                return {"message": "Missing authentification"}, 401
            auth = False
            try:
                auth = requests.get(url=AUTH_ENDPOINT,
                                    headers={"Authorization": request.headers["Authorization"],
                                             "X-Forwarded-For": request.headers["X-Forwarded-For"]}
                                    ).ok
            except Exception as auth_err:
                logger.error("Impossible to auth this request: %s", auth_err)
                return {"message": "Bad authentification"}, 401
            if auth:
                return route(*args, **kwargs)
            return {"message": "Bad authentification"}, 401

        return wrapper
