"""Module that creates an easier interface to netbox

The simplest usage is `netbox = maplib.Netbox()`.

From there, you can requests devices with a given tag using `.retrieve_hosts_with_tags(tag)`

Your microservice can also change the status of the device in netbox using `.update_device_status(hostname,status)`

Environment variables:

- `MAPLIB_NETBOX_URL`: specify a different complete url for netbox.

"""
import json
import os
from datetime import date

import pynetbox
import requests

from .maplog import Logger
from .mapvault import Vault

requests.urllib3.disable_warnings()


class Netbox():
    """netbox API object
    """

    def __init__(self, autoconfig=True, url="netbox", token=""):
        """creates a Netbox() instance

        Args:
            autoconfig (bool, optional): set to false if you configure using args. Defaults to True.
            url (str, optional): the url of netbox. Defaults to "netbox".
            token (str, optional): the token for netbox. Defaults to "".
        """
        self.logger = Logger("netbox")
        if "MAPLIB_NETBOX_URL" in os.environ:
            # this should be a complete url, with schema.
            self.url = os.getenv("MAPLIB_NETBOX_URL")
        elif not url.startswith("http://") and not url.startswith("https://"):
            self.url = "http://" + url
        else:
            self.url = url

        if autoconfig:
            v = Vault()
            if v.authenticate():
                secret = v.get_netbox_secrets()
                self.token = secret["token"]
            else:
                self.logger.critical(
                    "autoconfig is on, but vault is unreacheable")
        else:
            self.token = token
        self.nb = None

        # lambdas Getter
        self.get_full_dict = lambda x: dict(self.nb.dcim.devices.get(name=x))
        self.get_manufacturers = lambda: {x['name']: x['id'] for x in [
            dict(x) for x in self.nb.dcim.manufacturers.all()]}
        self.get_models = lambda: {x['model']: x['id'] for x in [
            dict(x) for x in self.nb.dcim.device_types.all()]}
        self.get_ips = lambda: {x['address']: x['id'] for x in [
            dict(x) for x in self.nb.ipam.ip_addresses.all()]}
        self.get_interface_for_device = lambda device_name, interface_name: {x['name']: x for x in [
            dict(x) for x in self.nb.dcim.interfaces.get(device=device_name, name=interface_name)]}

        # lambda setters (create)
        self.create_manufacturer = lambda manufacturer_name: dict(self.nb.dcim.manufacturers.get(
            name=self.nb.dcim.manufacturers.create(name=manufacturer_name, slug=manufacturer_name)))['id']

        # pynetbox connector
        self.nb = pynetbox.api(self.url, self.token)
        if "https" in self.url:
            self.nb.http_session.verify = False

        # http connector (due to some bugs when using callbacks on pynetbox requests)
        self.netbox_session = requests.Session()
        self.netbox_session.headers.update(
            {"Authorization": "Token {}".format(self.token)})

        # test
        try:
            self.nb.version
        except Exception as error:
            self.logger.error("could not connect to netbox : %s", error)

    def serialize_netbox_device(self, device: pynetbox.models.dcim.Devices):
        """converts a pynetbox object to a dict with simplified leaves

        Args:
            device (pynetbox.models.dcim.Devices): the og device object from pynetbox

        Returns:
            dict: a dict representing a device, with the keys `name`,`id`,`site`,`manufacturer`,`tags`,`ip` and `serial_number`

        """
        dev_dict = dict(device)
        try:
            dev = {"name": dev_dict["name"],
                   "id": dev_dict["id"],
                   "site": dev_dict["site"]["name"],
                   "manufacturer": dev_dict["device_type"]['manufacturer']['slug'],
                   "tags": [t["name"] for t in dev_dict["tags"]],
                   "status": dev_dict["status"]["value"]}

            try:
                dev['ip'] = dev_dict['primary_ip']["address"].split("/")[0]
            except Exception:
                self.logger.warning(
                    "The device %s does not have an IP assigned", dev)

            # add the serial number if it exists, None otherwise
            try:
                if dev_dict['serial'] == '':
                    dev["serial_number"] = None
                else:
                    dev["serial_number"] = dev_dict['serial']
            except Exception:
                pass
            return dev
        except Exception as error:
            self.logger.error(
                "Unable to parse data for device %s : %s", dev, error)
        return None

    def retrieve_hosts_with_tags(self, tag):
        """get the infos for each device in netbox who has a given tag

        Args:
            tag (str): the tag to filter devices

        Returns:
            dict: a dict of devices of type {'devicename':{'ip':"xx","tags":[]}}
        """
        hosts = {}
        for dev in self.nb.dcim.devices.filter(tag=tag):
            hosts[dev.name] = self.serialize_netbox_device(dev)
        return hosts

    def update_device_status(self, hostname, status):
        """Update a device status (by its hostname)

        Args:
            hostname (str): the name of the device to be updated
            status (str): the new status of the device, can be one from ['FAILED', 'ACTIVE', 'OFFLINE','PLANNED', 'STAGED', 'INVENTORY', 'DECOMMISSIONING']

        Returns:
            bool: True if ok, False otherwise
        """
        avail_status = ['FAILED', 'ACTIVE', 'OFFLINE',
                        'PLANNED', 'STAGED', 'INVENTORY', 'DECOMMISSIONING']
        try:
            device = self.nb.dcim.devices.get(name=hostname)
            if status.lower() == device.status.value:
                self.logger.debug(
                    "%s has already the %s status", hostname, status)
                return True
            if status.upper() in avail_status:
                self.netbox_session.patch(
                    "{}/api/dcim/devices/{}/".format(self.url, device.id), json={"status": status})
                self.logger.info("Status of %s updated", hostname)
            else:
                self.logger.error(
                    "This status is not available, please select one from Netbox.")
                return False
        except Exception as error:
            self.logger.error("could not parse data : %s", error)
            return False

        return True

    def find_device_by_name(self, hostname):
        """Return devices using their names

        Args:
            hostname (str): the name of the devices

        Returns:
            list: list of devices with the given name
        """
        try:
            named_device = self.nb.dcim.devices.get(name=hostname)
            return self.serialize_netbox_device(named_device)
        except Exception:
            return None

    def find_device_by_ip(self, ip_address):
        """returns the device which has this IP, none otherwise

        Args:
            ip_address (str): IP of the device to search for

        Returns:
            dict: dict of the device that has this IP, or None
        """
        ip = self.nb.ipam.ip_addresses.get(address=ip_address)
        if not ip:
            self.logger.error("IP %s does not exists", ip_address)
            return None
        try:
            device = self.nb.dcim.devices.get(ip.assigned_object.device.id)
            serialized_device = self.serialize_netbox_device(device)
            return serialized_device
        except Exception:
            return None

    def find_device_by_serial_number(self, serial_number):
        """return a device filtered by its serial

        Args:
            serial_number (str): the serial number

        Returns:
            list: list of devices matching
        """
        try:
            serial_device = self.nb.dcim.devices.get(serial=serial_number)
            return self.serialize_netbox_device(serial_device)
        except Exception:
            return None
