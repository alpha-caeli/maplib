
from random import choices, randint
from string import ascii_letters
import threading

import maplib


def test_mapnetbox():
    n = maplib.Netbox()
    assert isinstance(n.nb.status(), dict)

    unit_device_name = "unittest-"+"".join(choices(ascii_letters, k=10))
    dev_type = list(n.nb.dcim.device_types.all())[0].id
    dev_role = list(n.nb.dcim.device_roles.all())[0].id
    dev_site = list(n.nb.dcim.sites.all())[0].id
    if unit_device_name not in [t.slug for t in n.nb.extras.tags.all()]:
        n.nb.extras.tags.create(name=unit_device_name,
                                slug=unit_device_name, color="ff00ff")
    n.nb.dcim.devices.create(name=unit_device_name, status="active", serial=unit_device_name,
                             site=dev_site, device_type=dev_type, device_role=dev_role, tags=[{"slug": unit_device_name}])

    dev = n.find_device_by_name(unit_device_name)
    dev2 = n.find_device_by_serial_number(unit_device_name)
    dev3 = n.retrieve_hosts_with_tags(unit_device_name)

    assert isinstance(dev, dict)
    assert isinstance(dev2, dict)
    assert dev == dev2
    assert len(dev3) == 1
