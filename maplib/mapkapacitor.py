"""Module for working with kapacitor
"""
from urllib.parse import urljoin
import requests

from .maplog import Logger

logger = Logger("kapacitor")


def kapacitor_url(url): return urljoin(
    "http://kapacitor:9092/kapacitor/v1/", url)


def _get_tasks() -> list:
    """get the list of tasks

    Returns:
        list: [{},] format
    """
    tasks_request = requests.get(kapacitor_url("tasks"))
    if not tasks_request.ok:
        return []
    return tasks_request.json()["tasks"]


def _task_exists(task_name) -> bool:
    """does this task already exists ?

    Args:
        task_name (str): name of the task

    Returns:
        bool: True if it exists
    """
    tasks = _get_tasks()
    return task_name in [t["id"] for t in tasks]


def upsert_task(task_name: str, task_script: str, task_vars=None):
    """create/update a task by its name

    Args:
        task_name (str): name of the task, usually the microservice tag
        task_script (str): script of the task
        task_vars (dict, optional): vars dict. Defaults to None.
    """
    task_dict = {
        "id": "",
        "type": "stream",
        "script": "",
        "status": "enabled",
        "vars": {}
    }
    task_dict["id"] = task_name
    task_dict["script"] = task_script
    task_dict["vars"] = task_vars or {}
    if not _task_exists(task_name):
        r = requests.post(kapacitor_url("tasks"), json=task_dict)
    else:
        r = requests.patch(kapacitor_url(f"tasks/{task_name}"), json=task_dict)
    if not r.ok:
        logger.error(r.text)
    logger.debug(r.text)
