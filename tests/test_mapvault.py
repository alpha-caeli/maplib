import maplib


def test_auth():
    v = maplib.Vault()
    assert v.authenticate() == True


def test_secrets():
    v = maplib.Vault()
    secrets = v.list_secrets("services")
    assert isinstance(secrets, list)
    v.write_secret("unittest/secret", {"yolo": "true"})
    v.retrieve_secrets("unittest")
    s = v.read_secret("unittest/secret")
    assert isinstance(s, dict)
    v.delete_secret("unittest/secret")
    v.get_snmp_secrets("1.1.1.1")
    v.get_ssh_secrets("1.1.1.1")
