"""Module around hvac for retrieveing secrets from Vault

For informations about the privileged mode, look the "priveleged" section of the documentation.

Create the vault object : `v = maplib.Vault()`

Then the listed methods below will allow you to read specific secrets.
"""
import datetime
import os

import hvac
import requests

from .maplog import Logger

ROLE = "microservice"


def privileged():
    """Set the role to authenticate against Vault as privileged.
    """
    global ROLE
    ROLE = "privileged"


class Vault():
    """A Vault object that represents a connection to a vault endpoint.

    It should be used as a module to authenticate to vault & retrieve secrets.


    >>> import mapvault
    >>> v = mapvault.Vault()
    >>> if v.authenticate():
    >>>     secrets = v.retrieve_services_credentials()
    >>> print(secrets)
    """

    def __init__(self, host=None, port=None, path=None, role="microservice", quiet=False):

        self.logger = Logger("vault", "CRITICAL" if quiet else "INFO")
        self.host = os.getenv("VAULT_SERVICE_HOST", host)
        self.port = os.getenv("VAULT_SERVICE_PORT", port)
        self.client = None
        self.expiry_time = datetime.datetime.now()

        if self.host is None or self.port is None:
            self.host = "vault"
            self.port = "8200"

        # Adjust secrets path is modified by the user
        self.path = "secrets/"
        if path is not None:
            if path[-1] != "/":
                path += "/"
            self.path = path

        # Adjust Kubernetes role name if modified by user
        if ROLE != role:
            self.role = ROLE
        else:
            self.role = role

        # authenticate automagically
        self.authenticate()

    def authenticate(self):
        """Authenticate to vault using the k8 backend.

        It loads the service account token from /var.../token and tries to
        authenticate to vault.
        If any error rises, it will be printed.

        Returns:
            bool: True if authenticated, False otherwise
        """
        self.client = hvac.Client(url="http://" + self.host + ":" + self.port)
        try:
            init = self.client.sys.is_initialized()
            sealed = self.client.sys.is_sealed()
        except requests.exceptions.ConnectionError as conn_error:
            if 'connection refused' in str(conn_error).lower():
                self.logger.error(
                    "Vault actively refused the connection, it might be sealed")
            else:
                self.logger.error("Vault unreachable")
            return False

        if not init:
            self.logger.error("Vault is not initialized")
            return False
        if sealed:
            self.logger.error("Vault is sealed")
            return False

        try:
            with open('/var/run/secrets/kubernetes.io/serviceaccount/token', encoding='utf8') as tkn:
                jwt = tkn.read()
            self.client.auth_kubernetes(self.role, jwt)
        except FileNotFoundError:
            self.logger.error("No Service account token detected")
        except hvac.exceptions.VaultError as vault_error:
            self.logger.error(vault_error)
            return False
        if self.client.is_authenticated():

            # grab the token expiry date now
            self.fetch_token_validity()
            return True
        return False

    def fetch_token_validity(self):
        """Fetch and saves the current's token expiry time
        """
        expire_date = self.client.lookup_token()["data"]["expire_time"]
        iso_expiredeate = expire_date.split(".")[0]
        # remove 5s to avoid same-second-behavior
        self.expiry_time = datetime.datetime.fromisoformat(
            iso_expiredeate) - datetime.timedelta(seconds=5)

    def check_token(self):
        """renew the token against vault if we are after the expriy date
        """
        if datetime.datetime.now() > self.expiry_time:
            self.logger.debug("Token has expired")
            # re auth, as the token is not valid and cannot be renewed
            self.authenticate()
            self.fetch_token_validity()

    def list_secrets(self, path):
        """Returns a list of secrets from a given path. It does walk subdirs.

        Args:
            path (str): the parent path to list secrets from.
            Do not include the mount point of the secret engine.

        Returns:
            list: a list of str, representing the relative path of each secret.
        """
        try:
            self.check_token()
            # cleanup the path
            if len(path) > 0:
                if path[-1] == "/":
                    path = path[:-1]
                if path[0] != '/':
                    path = '/' + path

            # list secrets
            secrets_list = self.client.list(self.path + path)
            if secrets_list is None:
                return []
            secrets_list = secrets_list["data"]["keys"]

            # add the parent path
            secrets_list = [path + "/" + s for s in secrets_list]

            # if childs, recursive list
            for secret in secrets_list:
                if secret[-1] == "/":
                    secrets_list += self.list_secrets(secret)

            # remove any leftovers paths
            secrets_list = [s for s in secrets_list if s[-1] != '/']

            return secrets_list

        except hvac.exceptions.VaultError as listing_error:
            self.logger.error(
                "Fail during secrets listing for path %s : %s", path, str(listing_error))
            return []

    def read_secret(self, path):
        """read a secret's data given its path

        Args:
            path (str): the secret path

        Returns:
            dict: k:v dict like {foo:bar} if the secret exists, None otherwise.
        """
        self.check_token()
        try:
            secret = self.client.read(self.path + path)["data"]
        except TypeError:
            self.logger.error("No secrets at this path: %s", path)
            return None
        return secret

    def retrieve_secrets(self, path):
        """given a root path, returns all secrets.

        Args:
            path (str): the root path to use

        Returns:
            dict: k:v dict where k is the secret path, v is a dict of values from that path
        """
        self.check_token()
        secrets = self.list_secrets(path)

        values = {}
        for secret in secrets:
            val = self.read_secret(secret)
            values[secret] = val
        return values

    def write_secret(self, path, secret):
        """create a new secret

        Args:
            path (str): the path of the new secret
            secret (dict): the dict of values to store as the secret
        """
        try:
            self.check_token()
            self.client.secrets.kv.v1.create_or_update_secret(
                mount_point=self.path, path=path, secret=secret)
        except hvac.exceptions.Forbidden:
            self.logger.error(
                "You don't have the permissions to write on path %s !", path)
        except hvac.exceptions.VaultError as writing_error:
            self.logger.error(
                "Something failed when trying to write on path %s : %s", path, str(writing_error))

    def delete_secret(self, path):
        """delete a secret at a given path.

        This requires the client ot be linked
        to a policy that allows the 'delete' capability for the given path

        Args:
            path (str): the secret path to delete

        """
        try:
            self.check_token()
            self.client.secrets.kv.v1.delete_secret(
                mount_point=self.path, path=path)
        except hvac.exceptions.Forbidden:
            self.logger.error(
                "You don't have the permissions to delete the path %s !", path)
        except hvac.exceptions.VaultError as delete_error:
            self.logger.error(
                "Something failed when trying to delete the path %s : %s", path, str(delete_error))

    def get_netbox_secrets(self):
        """returns the token for netbox auth

        Returns:
            dict: the secret for netbox, containing the token
        """
        return self.read_secret("services/netbox/m2m")

    def get_minio_secrets(self):
        """returns the access and secret keys for minio

        Returns:
            dict: secret for minio
        """
        return self.read_secret("services/minio/m2m")

    def get_influx_secrets(self):
        """returns the user, pass and db for influx

        Returns:
            dict: secret for influx
        """
        return self.read_secret("services/influx/m2m")

    def get_grafana_secrets(self):
        """returns the user:pass for grafana

        Returns:
            dict: secret for grafana
        """
        return self.read_secret("services/grafana/m2m")

    def get_snmp_secrets(self, device_ip):
        """returns the snmp credentials for a given IP
        Args:
            device_ip (str): the ip of the device

        Returns:
            dict: the specific secret if any, else the generic snmp secret
        """
        if self.read_secret(f"snmp/{device_ip}"):
            return self.read_secret(f"snmp/{device_ip}")
        return self.read_secret("snmp/generic")

    def get_ssh_secrets(self, device_ip):
        """returns the ssh credentials for a given IP

        Args:
            device_ip (str): the ip of the device
        Returns:
            dict: the specific secret for the device (if any), else the generic SSH secret
        """
        if self.read_secret(f"ssh/{device_ip}"):
            return self.read_secret(f"ssh/{device_ip}")
        return self.read_secret("ssh/generic")
