"""
global wrapper for every service of MAP.

This init allows more readable imports from 3rd party modules,
like maplib.Vault() instead of maplib.mapvault.Vault().

You can request the version of maplib with `maplib.__version__`.
"""
__version__ = "1.7.1"

from random import choices

from . import maphubble as hubble
from . import security

from .mapgrafana import Grafana
from .maphasura import Hasura
from .mapinflux import Influx
from .maplog import Logger, sentry_setup
from .maploki import Loki
from .mapminio import Minio
from .mapnetbox import Netbox
from .mapvault import Vault, privileged
from .mapkapacitor import upsert_task


def init(ms_tag: str, set_privileged=False):
    """init the microservice. Creates the tag in netbox if necessary, sets up sentry if enabled, enables privileged mode in Vault

    Args:
        ms_tag (str): slug of the microservice, like my-special-microservice
        set_privileged (bool, optional): Set to True if you need privileged (write) access to Vault. Defaults to False.
    """
    init_logger = Logger("init-"+ms_tag)
    # setup vault
    if set_privileged:
        init_logger.debug("setting privileged account on Vault")
        privileged()

    # setup sentry
    sentry_setup(ms_tag)

    # and check if netbox has a tag
    n = Netbox()
    tags = [t.slug for t in n.nb.extras.tags.all()]

    if ms_tag not in tags:
        init_logger.debug("creating tag %s", ms_tag)
        try:
            name = " ".join([w.capitalize() for w in ms_tag.split("-")])
            n.nb.extras.tags.create(
                name=name, slug=ms_tag, color="".join(choices("3456789abc", k=6)))
        except Exception as init_err:
            init_logger.error(init_err)
