"""install module
"""
import setuptools

def read_requirements(requirements_path="requirements.txt") -> list:
    """reads requirements from a std requirements.txt file
    this avoids having a requirement file for testing/dockerfile
    and a separate list in this file

    Args:
        requirements_path (str, optional): path. Defaults to "requirements.txt".

    Returns:
        list: list of requirements for setuptools to ingest
    """
    with open(requirements_path) as req_fd:
        requirements = [l.strip() for l in req_fd.readlines()]
        return requirements


setuptools.setup(
    name="maplib",
    version="1.7.1",
    author="k0rventen",
    description="a wrapper for map services",
    python_requires='>=3.8',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    install_requires=read_requirements())
