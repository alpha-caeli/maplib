"""wrapper around minio

"""
import minio

from .maplog import Logger
from .mapvault import Vault


class Minio():
    """wrapper around minio
    """

    def __init__(self, endpoint="minio", port=9000, ssl=False,
                 autoconfig=True, accesskey="", secretkey=""):
        """sets up a minio client

        Args:
            endpoint (str, optional): the endpoint . Defaults to "minio".
            port (int, optional): [description]. Defaults to 9000.
            ssl (bool, optional): [description]. Defaults to False.
        """
        self.logger = Logger("minio")
        if autoconfig:
            vault = Vault()
            if vault.authenticate():
                self.logger.debug("fetching credentials from vault..")
                secr = vault.get_minio_secrets()
                self.client = minio.Minio(
                    f"{endpoint}:{port}", access_key=secr["accessKey"], secret_key=secr["secretKey"], secure=ssl)
            else:
                self.logger.critical(
                    "autoconfig is on but vault is unavailable !")
        else:
            self.client = minio.Minio(
                f"{endpoint}:{port}", access_key=accesskey, secret_key=secretkey, secure=ssl)

        # try
        try:
            self.client.list_buckets()
        except Exception as err:
            self.logger.error("could not access minio : %s", err)

    def list_buckets(self):
        """list the buckets on minio
        """
        return self.client.list_buckets()

    def list_files(self, bucket):
        """list all the files for a given bucket

        Args:
            bucket (str): bucket to use for listing files
        """
        return self.client.list_objects(bucket, recursive=True)

    def retrieve_file(self, bucket, path):
        """get a file from minio

        Args:
            bucket (str): bucket name
            path (str): path on minio where the file is stored

        """
        try:
            request = self.client.get_object(bucket, path)
            return request.data
        except minio.error.MinioException as err:
            self.logger.error("The file does not exists (%s)", err)
            return None

    def upload_file(self, local_path, bucket, path, **kwargs):
        """uploads a file to minio

        Args:
            local_path (str): local path to the file
            bucket (str): name of the bucket
            path (str): path on minio
        """
        try:
            self.client.fput_object(bucket, path, local_path, **kwargs)
        except FileNotFoundError:
            self.logger.error("Local file does not exists")
        except minio.error.MinioException as err:
            self.logger.error("The bucket does not exists. (%s)", err)

    def create_bucket(self, bucket):
        """create a bucket on minio

        Args:
            bucket (str): bucket name
        """
        try:
            self.client.make_bucket(bucket)
        except minio.error.MinioException:
            self.logger.debug("This bucket already exists and belongs to you.")
