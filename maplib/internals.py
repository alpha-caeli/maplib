"""
internal functions and utils for maplib
"""
import warnings
from datetime import datetime, timedelta
from functools import lru_cache, wraps


def deprecated(message):
    """
    decorator for deprecated functions that might be deleted onwards
    """
    def deprecated_decorator(func):
        def deprecated_func(*args, **kwargs):
            warnings.warn("{} is a deprecated function and might be deleted in a future version. {}".format(func.__name__, message),
                          category=DeprecationWarning,
                          stacklevel=2)
            warnings.simplefilter('default', DeprecationWarning)
            return func(*args, **kwargs)
        return deprecated_func
    return deprecated_decorator


def work_in_progress(message):
    """
    decorator for WIP that might change in future releases
    """
    def deprecated_decorator(func):
        def deprecated_func(*args, **kwargs):
            warnings.warn("{} is a WIP, expect things to change in future releases. {}".format(func.__name__, message),
                          category=FutureWarning,
                          stacklevel=2)
            warnings.simplefilter('default', FutureWarning)
            return func(*args, **kwargs)
        return deprecated_func
    return deprecated_decorator


def time_cache(seconds: int = 120, maxsize: int = 32):
    """a time senstitive version of @lru_cache. Cache expires after `seconds`.

    Args:
        seconds (int, optional): Lifetime of the cache entry, in seconds. Defaults to 120.
        maxsize (int, optional): Max number of entry in the lru_cache. Defaults to 32.
    """
    def wrapper_cache(func):
        func = lru_cache(maxsize=maxsize)(func)
        func.expire = datetime.utcnow() + timedelta(seconds=seconds)

        @wraps(func)
        def wrapped_func(*args, **kwargs):
            if datetime.utcnow() >= func.expire:
                func.cache_clear()
                func.expire = datetime.utcnow() + timedelta(seconds=seconds)

            return func(*args, **kwargs)

        return wrapped_func

    return wrapper_cache
