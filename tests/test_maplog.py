import threading
import time
import maplib
import os
import logging


def test_loglevel():
    os.environ["MAPLIB_LOG_LEVEL"] = "DEBUG"
    debug_logger = maplib.Logger("should_be_debug")
    assert debug_logger.level == logging.DEBUG

    os.environ["MAPLIB_LOG_LEVEL"] = "INFO"
    debug_logger = maplib.Logger("should_be_info")
    assert debug_logger.level == logging.INFO

    os.environ["MAPLIB_LOG_LEVEL"] = "ERROR"
    debug_logger = maplib.Logger("should_be_error")
    assert debug_logger.level == logging.ERROR

    os.environ["MAPLIB_LOG_LEVEL"] = "NOTALOGLEVEL"
    debug_logger = maplib.Logger("should_be_also_info")
    assert debug_logger.level == logging.INFO

    del os.environ["MAPLIB_LOG_LEVEL"]


def test_maplog():
    mylog = maplib.Logger("logtest")
    mylog.debug("Debug time !")
    mylog.info("Just a quick note")
    mylog.warning("We're reaching a limit..")
    mylog.error("Something's definitely not right")
    mylog.critical("If we're here, that's a problem.")
    try:
        1/0
    except:
        mylog.exception("wow we can't divide by 0")


def test_maplog_threaded():
    def thread_worker(_id):
        l = maplib.Logger("threaded-unittest")
        l.info("worker %s, this should be printed only once", _id)
        time.sleep(1)

    threads = []
    for i in range(3):
        thr = threading.Thread(target=thread_worker, args=(i,))
        thr.start()
        threads.append(thr)

    while any([x.is_alive() for x in threads]):
        time.sleep(1)
