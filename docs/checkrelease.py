"""CI check for consistency in the tags. 

Wheels are pushed to pypi when a tag is pusblished, but that means that the setup.py version, maplib's __version__ and git's tag version 
must be the same. If not, we might run into conflicts, or release a bad version to pypi. 

So we check if the 3 versions are the same, and exit with an error code accordingly.

A further check would be to only allow release candidates when tagging from a branch, and only stable releases when tagging from master.
"""
import os
import re
import logging

log = logging.getLogger()
stdout_handler = logging.StreamHandler()
stdout_handler.setFormatter(logging.Formatter("%(levelname)s :: %(message)s"))
log.addHandler(stdout_handler)
log.setLevel("INFO")

version_regex = r"version.*[\"'](.*)[\"']"

def retrieve_setup_version():
    with open("setup.py") as f:
        version = re.search(version_regex, f.read())
        if version:
            return version.groups()[0]


def retrieve_maplib_version():
    with open("maplib/__init__.py") as f:
        version = re.search(version_regex, f.read())
        if version:
            return version.groups()[0]


def retrieve_tag_version():
    version = os.getenv("CI_COMMIT_TAG","")
    return version


def check_branch():
    log.info("Checking branches..")
    # grab the branch we are on
    branch = os.getenv("CI_COMMIT_REF_SLUG").replace("v", "").replace(".","-")
    tag = os.getenv("CI_COMMIT_TAG").replace("v", "").replace(".","-")

    # master must not contain pre release identifiers
    if branch == "master":
        if all([not x in tag for x in ["dev", "rc", "a", "b", "post"]]):
            log.info("Stable release on the master branch")
            return True
        else:
            log.error(
                "We are on master, but the tag refers to a release candidate'{}'!".format(tag))
            return False
    else:
        # others must be rc
        if branch in tag:
            log.info("Branch {} with tag {}".format(branch, tag))
            return True
        else:
            log.error(
                "Mismatch between the branch '{}' and the tag '{}'!".format(branch, tag))
            return False


def check_versions():
    log.info("Checking versions..")
    setup = retrieve_setup_version()
    maplib = retrieve_maplib_version()
    tag = retrieve_tag_version()
    if setup == maplib == tag.replace("v", ""):
        log.info("Tags are all the same!")
        return True
    log.error("Tags are not coherent! setup is {}, maplib is {}, tag is {}".format(
        setup, maplib, tag))
    return False


if __name__ == "__main__":
    if check_versions() and check_branch():
        log.info("Exiting with status code 0 !")
        exit(0)
    log.error("Exiting with status code 1 !")
    exit(1)
